****************
Game environment
****************

Environment settings
====================

.. c:function:: vcmpError PluginFuncs.SetServerOption(vcmpServerOption option, uint8_t toggle)

   Sets a boolean server flag.

   :param vcmpServerOption option: The option to toggle on or off

   :param uint8_t toggle: 1 to enable, 0 to disable

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if the server does not
            support the option given.

   :see also: :c:type:`vcmpServerOption`

.. c:function:: uint8_t PluginFuncs.GetServerOption(vcmpServerOption option)

   Checks if a boolean server flag is enabled.

   :param vcmpServerOption option: The option to check

   :return: 1 if enabled, 0 if disabled

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if the server does not
            support the option given.

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: void PluginFuncs.SetWorldBounds(float maxX, minX, maxY, minY)

   Sets the boundaries of the game world. Players who attempt to travel beyond
   these boundaries will automatically and uncontrollably jump toward the
   acceptable play area.

   :param float maxX: The maximum X coordinate of the new play area
   :param float minX: The minimum X coordinate of the new play area
   :param float maxY: The maximum Y coordinate of the new play area
   :param float minY: The minimum Y coordinate of the new play area

   .. note::

      Although this plugin call always succeeds, VC:MP prevents any entity
      from traveling outside the boundaries
      :samp:`(-2350.0, -1950.0, -200.0), (1550.0, 1950.0, 20000.0)` as
      traveling outside these bounds causes the game to crash.

.. c:function:: void PluginFuncs.GetWorldBounds(float* maxXOut, float* minXOut, float* maxYOut, float* minYOut)

   Gets the boundaries of the game world.

   :param float* maxXOut: Pointer to a float to store the maximum X coordinate in
   :param float* minXOut: Pointer to a float to store the minimum X coordinate in
   :param float* maxYOut: Pointer to a float to store the maximum Y coordinate in
   :param float* minYOut: Pointer to a float to store the minimum Y coordinate in

.. c:function:: void PluginFuncs.SetWastedSettings(uint32_t deathTimer, uint32_t fadeTimer, float fadeInSpeed, float fadeOutSpeed, uint32_t fadeColour, uint32_t corpseFadeStart, uint32_t corpseFadeTime)

.. c:function:: void PluginFuncs.GetWastedSettings(uint32_t* deathTimerOut, uint32_t* fadeTimerOut, float* fadeInSpeedOut, float* fadeOutSpeedOut, uint32_t* fadeColourOut, uint32_t* corpseFadeStartOut, uint32_t* corpseFadeTimeOut)

.. c:function:: void PluginFuncs.SetTimeRate(int32_t timeRate)

   :param int32_t timeRate: The number of milliseconds it takes for one game
                            second to elapse, or 0 to stop time

   :default: 0 (time does not move by default)

.. c:function:: int32_t PluginFuncs.GetTimeRate(void)

   :return: The number of milliseconds it takes for one game second to elapse

.. c:function:: void PluginFuncs.SetHour(int32_t hour)

   :param int32_t hour: The new in-game hour
   :default: 12

.. c:function:: int32_t PluginFuncs.GetHour(void)

   :return: The current in-game hour

.. c:function:: void PluginFuncs.SetMinute(int32_t minute)

   :param int32_t hour: The new in-game minute
   :default: 0

.. c:function:: int32_t PluginFuncs.GetMinute(void)

   :return: The current in-game minute

.. c:function:: void PluginFuncs.SetWeather(int32_t weather)

   :param int32_t hour: The new in-game weather
   :see also: :ref:`weather`
   :default: 0

.. c:function:: int32_t PluginFuncs.GetWeather(void)

   :return: The current in-game weather
   :see also: :ref:`weather`

.. c:function:: void PluginFuncs.SetGravity(float gravity)

   :param float gravity: The new gravitational rate of acceleration
   :default: 0.008

.. c:function:: float PluginFuncs.GetGravity(void)

   :return: The current gravitational rate of acceleration

.. c:function:: void PluginFuncs.SetGameSpeed(float gameSpeed)

   :param float gameSpeed: The new game speed
   :default: 1.0

.. c:function:: float PluginFuncs.GetGameSpeed(void)

   :return: The current game speed

.. c:function:: void PluginFuncs.SetWaterLevel(float waterLevel)

   :param float waterLevel: The new water level
   :default: 6.0

.. c:function:: float PluginFuncs.GetWaterLevel(void)

   :return: The current water level

.. c:function:: void PluginFuncs.SetMaximumFlightAltitude(float height)

   Sets the height at which helicopters and planes will stop ascending.

   :param float height: The new maximum flight altitude
   :default: 10000.0

.. c:function:: float PluginFuncs.GetMaximumFlightAltitude(void)

   :return: The current maximum flight altitude

.. c:function:: void PluginFuncs.SetKillCommandDelay(int32_t delay)

   Sets how many milliseconds must pass after a player uses the /kill command
   before they actually die. If higher than 25000ms (25 seconds), players will
   not be able to kill themselves using the built-in /kill command.

   :param int32_t delay: The new command delay
   :default: 0

.. c:function:: int32_t PluginFuncs.GetKillCommandDelay(void)

   :return: The current kill command delay

.. c:function:: void PluginFuncs.SetVehiclesForcedRespawnHeight(float height)

   Sets the height above which unoccupied vehicles will respawn by themselves.
   Used to prevent vehicles from becoming inaccessible if players fly them
   too high to be streamed to other players and then quit the server.

   :param float height: The new forced respawn height
   :default: 200.0

.. c:function:: float PluginFuncs.GetVehiclesForcedRespawnHeight(void)

   :return: The current forced respawn height

World interactions
==================

.. c:function:: vcmpError PluginFuncs.CreateExplosion(int32_t worldId, int32_t type, float x, float y, float z, int32_t responsiblePlayerId, uint8_t atGroundLevel)

   Creates an explosion at a certain location and optionally attributes the
   explosion to a particular player.

   :param int32_t worldId: The :term:`virtual world` to create the explosion in
   :param int32_t type: The :ref:`explosion type <explosion-types>`
   :param float x: The X coordinate for the explosion
   :param float y: The Y coordinate for the explosion
   :param float z: The Z coordinate for the explosion; ignored if
                   *atGroundLevel* is true

   :param int32_t responsiblePlayerId: The ID of the player to attribute the
                                       explosion to, or -1 to attribute to no
                                       particular player

   :param uint8_t atGroundLevel: If 1, creates an explosion on the ground
                                 regardless of the given Z coordinate. Otherwise,
                                 creates explosion at the given Z coordinate.

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if type is less than 0 or
            greater than 11.

            :c:data:`vcmpErrorNoSuchEntity` if no player with the given player
            ID exists.
    
   .. note::

      Explosions can be created for a specific player only by creating an
      explosion in their unique world. See
      :c:func:`PluginFuncs.GetPlayerUniqueWorld()`.

.. c:function:: vcmpError PluginFuncs.PlaySound(int32_t worldId, int32_t soundId, float x, float y, float z)

   Plays a sound at a certain location.

   :param int32_t worldId: The :term:`virtual world` to create the sound in
   :param int32_t soundId: The ID of the sound to play
   :param float x: The X coordinate for the sound
   :param float y: The Y coordinate for the sound
   :param float z: The Z coordinate for the sound
    
   .. note::

      Sounds can be created for a specific player only by creating an
      sound in their unique world. See
      :c:func:`PluginFuncs.GetPlayerUniqueWorld()`.
    
   .. note::

      If either the X, Y or Z coordinate are set to NaN, the sound will not
      play at a particular position in the world and will instead play at full
      volume regardless of where players are.

.. c:function:: void PluginFuncs.HideMapObject(int32_t modelId, int16_t tenthX, int16_t tenthY, int16_t tenthZ)

   Hides a default object in the game world.

   :param int32_t modelId: The ID of the object model to hide
   :param int16_t tenthX: The X coordinate for the object, multiplied by 10 and
                          rounded off as a 16-bit integer.

   :param int16_t tenthY: The Y coordinate for the object, multiplied by 10 and
                          rounded off as a 16-bit integer.

   :param int16_t tenthZ: The Z coordinate for the object, multiplied by 10 and
                          rounded off as a 16-bit integer.

   .. note::

      If you intend to hide every single Vice City map object, you should
      create an empty file named :samp:`store/maps/hidevicecity.txt` in the
      server directory instead.

.. c:function:: void PluginFuncs.ShowMapObject(int32_t modelId, int16_t tenthX, int16_t tenthY, int16_t tenthZ)

   Unhides a default object in the game world.

   :param int32_t modelId: The ID of the object model to unhide
   :param int16_t tenthX: The X coordinate for the object, multiplied by 10 and
                          rounded off as a 16-bit integer.

   :param int16_t tenthY: The Y coordinate for the object, multiplied by 10 and
                          rounded off as a 16-bit integer.

   :param int16_t tenthZ: The Z coordinate for the object, multiplied by 10 and
                          rounded off as a 16-bit integer.

.. c:function:: void PluginFuncs.ShowAllMapObjects(void)

   Unhides any and all default game map objects that were previously hidden
   by :c:func:`PluginFuncs.HideMapObject()`.

Map/radar blips
===============

.. c:function:: int32_t PluginFuncs.CreateCoordBlip(int32_t index, int32_t world, float x, float y, float z, int32_t scale, uint32_t colour, int32_t sprite)

   Creates a map blip that appears on the radar and main menu map.

   :param int32_t index: An index for the blip; if the index is already used by
                         another map blip, the server will try to use another
                         index instead

   :param int32_t world: The virtual world to create the icon in
   :param float x: The X coordinate for the map icon
   :param float y: The Y coordinate for the map icon
   :param float z: The Z coordinate for the map icon
   :param int32_t scale: The scale of the blip, between 0 and 2 inclusive
   :param uint32_t colour: The colour for the map icon, if applicable
   :param int32_t sprite: The :ref:`type of map icon <map-icons>` to create,
                          or 0 to create a plain coloured blip

   :return: The actual index assigned to the map blip
   :raises: :c:data:`vcmpErrorPoolExhausted` if there is
            :ref:`no space <entity-limits>` for more map blips.

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.DestroyCoordBlip(int32_t index)

   Deletes a map blip.

   :param int32_t index: The index of the map blip to delete
   :raises: :c:data:`vcmpErrorNoSuchEntity` if no such map blip exists.

.. c:function:: vcmpError PluginFuncs.GetCoordBlipInfo(int32_t index, int32_t* worldOut, float* xOut, float* yOUt, float* zOut, int32_t* scaleOut, uint32_t* colourOut, int32_t* spriteOut)

   Gets information about a map blip.

   :param int32_t index: The index of the map blip to get information for
   :param int32_t* worldOut: A pointer to an int32_t to store the blip's world in
   :param float* xOut: A pointer to a float to store the blip's X coordinate in
   :param float* yOut: A pointer to a float to store the blip's Y coordinate in
   :param float* zOut: A pointer to a float to store the blip's Z coordinate in
   :param int32_t* scaleOut: A pointer to an int32_t to store the blip's scale in
   :param uint32_t* colourOut: A pointer to an uint32_t to store the blip's colour in
   :param int32_t* spriteOut: A pointer to an int32_t to store the blip's sprite in

   :raises: :c:data:`vcmpErrorNoSuchEntity` if no such map blip exists.

Radio streams
=============

.. c:function:: vcmpError PluginFuncs.AddRadioStream(int32_t radioId, const char* radioName, const char* radioUrl, uint8_t isListed)

   Creates a radio stream pointing to an internet audio stream that can be
   used as an in-game radio station.

   :param int32_t radioId: An index for the stream; if the index is already used
                           by another radio stream, the server will try to use
                           another index instead

   :type radioName: const char*
   :param radioName: The name of the radio station to show when changing
                     stations in a vehicle

   :type radioUrl: const char*
   :param radioUrl: The URL for the radio stream

   :param uint8_t isListed: 1 if this station should appear in the in-vehicle
                            radio station scroll cycle. 0 if the server should
                            be required to assign this radio station to a
                            vehicle.

   :return: The actual index assigned to the map blip
   :raises: :c:data:`vcmpErrorPoolExhausted` if there is
            :ref:`no space <entity-limits>` for more radio stations.

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.RemoveRadioStream(int32_t radioId)

   Deletes a radio stream.

   :param int32_t index: The index of the radio stream to delete
   :raises: :c:data:`vcmpErrorNoSuchEntity` if no such radio stream exists.

Entity status
=============

.. c:function:: uint8_t PluginFuncs.CheckEntityExists(vcmpEntityPool entityPool, int32_t index)

   :param vcmpEntityPool entityPool: The entity pool to check against
   :param int32_t index: The index of the entity to check for the existence of
   
   :return: 1 if an entity with the given index exists in the given pool.
            0 otherwise.