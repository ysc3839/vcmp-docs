***********************
Pickups and checkpoints
***********************

Pickups
=======

.. c:function:: int32_t PluginFuncs.CreatePickup(int32_t modelIndex, int32_t world, int32_t quantity, float x, float y, float z, int32_t alpha, uint8_t isAutomatic)

   TODO: func desc

   :param int32_t modelIndex: TODO: arg desc
   :param int32_t world: TODO: arg desc
   :param int32_t quantity: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param int32_t alpha: TODO: arg desc
   :param uint8_t isAutomatic: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`vcmpErrorPoolExhausted` if TODO

.. c:function:: vcmpError PluginFuncs.DeletePickup(int32_t pickupId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint8_t PluginFuncs.IsPickupStreamedForPlayer(int32_t pickupId, int32_t playerId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPickupWorld(int32_t pickupId, int32_t world)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc
   :param int32_t world: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPickupWorld(int32_t pickupId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPickupAlpha(int32_t pickupId, int32_t alpha)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc
   :param int32_t alpha: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPickupAlpha(int32_t pickupId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPickupIsAutomatic(int32_t pickupId, uint8_t toggle)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc
   :param uint8_t toggle: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint8_t PluginFuncs.IsPickupAutomatic(int32_t pickupId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPickupAutoTimer(int32_t pickupId, uint32_t durationMillis)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc
   :param uint32_t durationMillis: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint32_t PluginFuncs.GetPickupAutoTimer(int32_t pickupId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc

   :return: uint32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.RefreshPickup(int32_t pickupId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetPickupPosition(int32_t pickupId, float x, float y, float z)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetPickupPosition(int32_t pickupId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPickupModel(int32_t pickupId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPickupQuantity(int32_t pickupId)

   TODO: func desc

   :param int32_t pickupId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Checkpoints
===========

.. c:function:: int32_t PluginFuncs.CreateCheckPoint(int32_t playerId, int32_t world, uint8_t isSphere, float x, float y, float z, int32_t red, int32_t green, int32_t blue, int32_t alpha, float radius)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t world: TODO: arg desc
   :param uint8_t isSphere: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param int32_t red: TODO: arg desc
   :param int32_t green: TODO: arg desc
   :param int32_t blue: TODO: arg desc
   :param int32_t alpha: TODO: arg desc
   :param float radius: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`vcmpErrorPoolExhausted, vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.DeleteCheckPoint(int32_t checkPointId)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint8_t PluginFuncs.IsCheckPointStreamedForPlayer(int32_t checkPointId, int32_t playerId)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: uint8_t PluginFuncs.IsCheckPointSphere(int32_t checkPointId)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetCheckPointWorld(int32_t checkPointId, int32_t world)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc
   :param int32_t world: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetCheckPointWorld(int32_t checkPointId)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetCheckPointColour(int32_t checkPointId, int32_t red, int32_t green, int32_t blue, int32_t alpha)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc
   :param int32_t red: TODO: arg desc
   :param int32_t green: TODO: arg desc
   :param int32_t blue: TODO: arg desc
   :param int32_t alpha: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetCheckPointColour(int32_t checkPointId, int32_t* redOut, int32_t* greenOut, int32_t* blueOut, int32_t* alphaOut)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc
   :param int32_t* redOut: TODO: arg desc
   :param int32_t* greenOut: TODO: arg desc
   :param int32_t* blueOut: TODO: arg desc
   :param int32_t* alphaOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetCheckPointPosition(int32_t checkPointId, float x, float y, float z)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetCheckPointPosition(int32_t checkPointId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetCheckPointRadius(int32_t checkPointId, float radius)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc
   :param float radius: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: float PluginFuncs.GetCheckPointRadius(int32_t checkPointId)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc

   :return: float TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetCheckPointOwner(int32_t checkPointId)

   TODO: func desc

   :param int32_t checkPointId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.
