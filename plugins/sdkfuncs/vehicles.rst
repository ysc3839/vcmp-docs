*****************
Vehicle functions
*****************

.. Vehicle streaming
.. =================

.. Vehicle position and rotation
.. =============================

.. Vehicle status
.. ==============

.. c:function:: int32_t PluginFuncs.CreateVehicle(int32_t modelIndex, int32_t world, float x, float y, float z, float angle, int32_t primaryColour, int32_t secondaryColour)

   TODO: func desc

   :param int32_t modelIndex: TODO: arg desc
   :param int32_t world: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param float angle: TODO: arg desc
   :param int32_t primaryColour: TODO: arg desc
   :param int32_t secondaryColour: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorArgumentOutOfBounds, vcmpErrorPoolExhausted` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.DeleteVehicle(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleOption(int32_t vehicleId, vcmpVehicleOption option, uint8_t toggle)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param vcmpVehicleOption option: TODO: arg desc
   :param uint8_t toggle: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: uint8_t PluginFuncs.GetVehicleOption(int32_t vehicleId, vcmpVehicleOption option)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param vcmpVehicleOption option: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetVehicleSyncSource(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpVehicleSync PluginFuncs.GetVehicleSyncType(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: vcmpVehicleSync TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: uint8_t PluginFuncs.IsVehicleStreamedForPlayer(int32_t vehicleId, int32_t playerId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetVehicleWorld(int32_t vehicleId, int32_t world)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t world: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetVehicleWorld(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetVehicleModel(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetVehicleOccupant(int32_t vehicleId, int32_t slotIndex)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t slotIndex: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.RespawnVehicle(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleImmunityFlags(int32_t vehicleId, uint32_t immunityFlags)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param uint32_t immunityFlags: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint32_t PluginFuncs.GetVehicleImmunityFlags(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: uint32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.ExplodeVehicle(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint8_t PluginFuncs.IsVehicleWrecked(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetVehiclePosition(int32_t vehicleId, float x, float y, float z, uint8_t removeOccupants)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param uint8_t removeOccupants: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehiclePosition(int32_t vehicleId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleRotation(int32_t vehicleId, float x, float y, float z, float w)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param float w: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleRotationEuler(int32_t vehicleId, float x, float y, float z)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehicleRotation(int32_t vehicleId, float* xOut, float* yOut, float* zOut, float* wOut)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc
   :param float* wOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehicleRotationEuler(int32_t vehicleId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleSpeed(int32_t vehicleId, float x, float y, float z, uint8_t add, uint8_t relative)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param uint8_t add: TODO: arg desc
   :param uint8_t relative: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehicleSpeed(int32_t vehicleId, float* xOut, float* yOut, float* zOut, uint8_t relative)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc
   :param uint8_t relative: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleTurnSpeed(int32_t vehicleId, float x, float y, float z, uint8_t add, uint8_t relative)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param uint8_t add: TODO: arg desc
   :param uint8_t relative: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehicleTurnSpeed(int32_t vehicleId, float* xOut, float* yOut, float* zOut, uint8_t relative)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc
   :param uint8_t relative: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleSpawnPosition(int32_t vehicleId, float x, float y, float z)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehicleSpawnPosition(int32_t vehicleId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleSpawnRotation(int32_t vehicleId, float x, float y, float z, float w)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param float w: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleSpawnRotationEuler(int32_t vehicleId, float x, float y, float z)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehicleSpawnRotation(int32_t vehicleId, float* xOut, float* yOut, float* zOut, float* wOut)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc
   :param float* wOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehicleSpawnRotationEuler(int32_t vehicleId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehicleIdleRespawnTimer(int32_t vehicleId, uint32_t millis)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param uint32_t millis: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint32_t PluginFuncs.GetVehicleIdleRespawnTimer(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: uint32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetVehicleHealth(int32_t vehicleId, float health)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float health: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: float PluginFuncs.GetVehicleHealth(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: float TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetVehicleColour(int32_t vehicleId, int32_t primaryColour, int32_t secondaryColour)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t primaryColour: TODO: arg desc
   :param int32_t secondaryColour: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetVehicleColour(int32_t vehicleId, int32_t* primaryColourOut, int32_t* secondaryColourOut)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t* primaryColourOut: TODO: arg desc
   :param int32_t* secondaryColourOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorNullArgument` if TODO

.. c:function:: vcmpError PluginFuncs.SetVehiclePartStatus(int32_t vehicleId, int32_t partId, int32_t status)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t partId: TODO: arg desc
   :param int32_t status: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetVehiclePartStatus(int32_t vehicleId, int32_t partId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t partId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetVehicleTyreStatus(int32_t vehicleId, int32_t tyreId, int32_t status)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t tyreId: TODO: arg desc
   :param int32_t status: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetVehicleTyreStatus(int32_t vehicleId, int32_t tyreId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t tyreId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetVehicleDamageData(int32_t vehicleId, uint32_t damageData)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param uint32_t damageData: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint32_t PluginFuncs.GetVehicleDamageData(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: uint32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetVehicleRadio(int32_t vehicleId, int32_t radioId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t radioId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetVehicleRadio(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.GetVehicleTurretRotation(int32_t vehicleId, float* horizontalOut, float* verticalOut)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param float* horizontalOut: TODO: arg desc
   :param float* verticalOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

Vehicle handling
================

.. c:function:: void PluginFuncs.ResetAllVehicleHandlings(void)

   TODO: func desc


.. c:function:: uint8_t PluginFuncs.ExistsHandlingRule(int32_t modelIndex, int32_t ruleIndex)

   TODO: func desc

   :param int32_t modelIndex: TODO: arg desc
   :param int32_t ruleIndex: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: vcmpError PluginFuncs.SetHandlingRule(int32_t modelIndex, int32_t ruleIndex, double value)

   TODO: func desc

   :param int32_t modelIndex: TODO: arg desc
   :param int32_t ruleIndex: TODO: arg desc
   :param double value: TODO: arg desc

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: double PluginFuncs.GetHandlingRule(int32_t modelIndex, int32_t ruleIndex)

   TODO: func desc

   :param int32_t modelIndex: TODO: arg desc
   :param int32_t ruleIndex: TODO: arg desc

   :return: double TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.ResetHandlingRule(int32_t modelIndex, int32_t ruleIndex)

   TODO: func desc

   :param int32_t modelIndex: TODO: arg desc
   :param int32_t ruleIndex: TODO: arg desc

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: vcmpError PluginFuncs.ResetHandling(int32_t modelIndex)

   TODO: func desc

   :param int32_t modelIndex: TODO: arg desc

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: uint8_t PluginFuncs.ExistsInstHandlingRule(int32_t vehicleId, int32_t ruleIndex)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t ruleIndex: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetInstHandlingRule(int32_t vehicleId, int32_t ruleIndex, double value)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t ruleIndex: TODO: arg desc
   :param double value: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: double PluginFuncs.GetInstHandlingRule(int32_t vehicleId, int32_t ruleIndex)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t ruleIndex: TODO: arg desc

   :return: double TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.ResetInstHandlingRule(int32_t vehicleId, int32_t ruleIndex)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc
   :param int32_t ruleIndex: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: vcmpError PluginFuncs.ResetInstHandling(int32_t vehicleId)

   TODO: func desc

   :param int32_t vehicleId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO
