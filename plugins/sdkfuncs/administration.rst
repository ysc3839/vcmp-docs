*********************
Server administration
*********************

.. c:function:: uint8_t PluginFuncs.IsPlayerAdmin(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerAdmin(int32_t playerId, uint8_t toggle)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param uint8_t toggle: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetPlayerIP(int32_t playerId, char* buffer, size_t size)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param char* buffer: TODO: arg desc
   :param size_t size: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorNullArgument, vcmpErrorBufferTooSmall` if TODO

.. c:function:: vcmpError PluginFuncs.GetPlayerUID(int32_t playerId, char* buffer, size_t size)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param char* buffer: TODO: arg desc
   :param size_t size: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorNullArgument, vcmpErrorBufferTooSmall` if TODO

.. c:function:: vcmpError PluginFuncs.GetPlayerUID2(int32_t playerId, char* buffer, size_t size)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param char* buffer: TODO: arg desc
   :param size_t size: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorNullArgument, vcmpErrorBufferTooSmall` if TODO

.. c:function:: vcmpError PluginFuncs.KickPlayer(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.BanPlayer(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: void PluginFuncs.BanIP(char* ipAddress)

   TODO: func desc

   :param char* ipAddress: TODO: arg desc

.. c:function:: uint8_t PluginFuncs.UnbanIP(char* ipAddress)

   TODO: func desc

   :param char* ipAddress: TODO: arg desc

   :return: uint8_t TODO: ret desc

.. c:function:: uint8_t PluginFuncs.IsIPBanned(char* ipAddress)

   TODO: func desc

   :param char* ipAddress: TODO: arg desc

   :return: uint8_t TODO: ret desc
