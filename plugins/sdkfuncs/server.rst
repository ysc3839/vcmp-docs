***************
Server settings
***************

.. c:function:: vcmpError PluginFuncs.GetServerSettings(ServerSettings* settings)

   Stores some server properties in the given struct.

   :param ServerSettings* settings: A pointer to a :c:type:`ServerSettings`
                                    struct to store the settings in

   :raises: :c:data:`vcmpErrorNullArgument` if the *settings* pointer is null

.. c:function:: vcmpError PluginFuncs.SetServerName(const char* text)

   Changes the server's name.

   :type text: const char*
   :param text: The server's new name

   :raises: :c:data:`vcmpErrorNullArgument` if *text* is null

            :c:data:`vcmpErrorTooLargeInput` if *text* is longer than 127
            characters

   .. note::

      If the server is launched with the :samp:`-appendname` argument, the
      server name will still have that component appended.

.. c:function:: vcmpError PluginFuncs.GetServerName(char* buffer, size_t size)

   Gets the server's name.

   :param char* buffer: A pointer to a char buffer to store the server name in

   :param size_t size: The size of the buffer

   :raises: :c:data:`vcmpErrorNullArgument` if *buffer* is null

            :c:data:`vcmpErrorBufferTooSmall` if *buffer* is too small to fit
            the server name

.. c:function:: vcmpError PluginFuncs.SetMaxPlayers(uint32_t maxPlayers)

   Sets the maximum number of players that can join the server.

   :param uint32_t maxPlayers: The new player limit

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if *maxPlayers* is smaller
            than 1 or larger than 100

   .. note::

      If the server is launched with the :samp:`-maxplayers` argument, the
      server will silently throttle the maximum number of players to this value.

.. c:function:: uint32_t PluginFuncs.GetMaxPlayers(void)

   :return: The maximum number of players that can join the server.

.. c:function:: vcmpError PluginFuncs.SetServerPassword(const char* password)

   Sets or unsets the password needed to join the server.

   :type password: const char*
   :param password: The new password

   :raises: :c:data:`vcmpErrorNullArgument` if *password* is null

            :c:data:`vcmpErrorTooLargeInput` if *password* is longer than 64
            characters

   .. note::

      To remove the server password, pass an empty string, not a null pointer.

.. c:function:: vcmpError PluginFuncs.GetServerPassword(char* buffer, size_t size)

   Gets the password needed to join the server.

   :param char* buffer: A pointer to a char buffer to store the password in

   :param size_t size: The size of the buffer

   :raises: :c:data:`vcmpErrorNullArgument` if *buffer* is null

            :c:data:`vcmpErrorBufferTooSmall` if *buffer* is too small to fit
            the password

.. c:function:: vcmpError PluginFuncs.SetGameModeText(const char* gameMode)

   Sets the gamemode name advertised to the server browser.

   :type gameMode: const char*
   :param gameMode: The new gamemode name

   :raises: :c:data:`vcmpErrorNullArgument` if *gameMode* is null

            :c:data:`vcmpErrorTooLargeInput` if *gameMode* is longer than 64
            characters

.. c:function:: vcmpError PluginFuncs.GetServerGameModeText(char* buffer, size_t size)

   Gets the gamemode name advertised to the server browser.

   :param char* buffer: A pointer to a char buffer to store the gamemode name in

   :param size_t size: The size of the buffer

   :raises: :c:data:`vcmpErrorNullArgument` if *buffer* is null

            :c:data:`vcmpErrorBufferTooSmall` if *buffer* is too small to fit
            the gamemode name

.. c:function:: void PluginFuncs.ShutdownServer(void)

   Gracefully stops the server.
