*********************
Server-side key binds
*********************

.. c:function:: int32_t PluginFuncs.GetKeyBindUnusedSlot(void)

   TODO: func desc


   :return: int32_t TODO: ret desc
   :raises: :c:data:`-1 == vcmpEntityNone` if TODO

.. c:function:: vcmpError PluginFuncs.GetKeyBindData(int32_t bindId, uint8_t* isCalledOnReleaseOut, int32_t* keyOneOut, int32_t* keyTwoOut, int32_t* keyThreeOut)

   TODO: func desc

   :param int32_t bindId: TODO: arg desc
   :param uint8_t* isCalledOnReleaseOut: TODO: arg desc
   :param int32_t* keyOneOut: TODO: arg desc
   :param int32_t* keyTwoOut: TODO: arg desc
   :param int32_t* keyThreeOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.RegisterKeyBind(int32_t bindId, uint8_t isCalledOnRelease, int32_t keyOne, int32_t keyTwo, int32_t keyThree)

   TODO: func desc

   :param int32_t bindId: TODO: arg desc
   :param uint8_t isCalledOnRelease: TODO: arg desc
   :param int32_t keyOne: TODO: arg desc
   :param int32_t keyTwo: TODO: arg desc
   :param int32_t keyThree: TODO: arg desc

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: vcmpError PluginFuncs.RemoveKeyBind(int32_t bindId)

   TODO: func desc

   :param int32_t bindId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: void PluginFuncs.RemoveAllKeyBinds(void)

   TODO: func desc
