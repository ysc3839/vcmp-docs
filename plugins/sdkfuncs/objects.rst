****************
Object functions
****************

.. Object streaming
.. ================

.. Object position and rotation
.. ============================

.. Object status
.. =============

.. c:function:: int32_t PluginFuncs.CreateObject(int32_t modelIndex, int32_t world, float x, float y, float z, int32_t alpha)

   TODO: func desc

   :param int32_t modelIndex: TODO: arg desc
   :param int32_t world: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param int32_t alpha: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorPoolExhausted` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.DeleteObject(int32_t objectId)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint8_t PluginFuncs.IsObjectStreamedForPlayer(int32_t objectId, int32_t playerId)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetObjectModel(int32_t objectId)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetObjectWorld(int32_t objectId, int32_t world)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param int32_t world: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetObjectWorld(int32_t objectId)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetObjectAlpha(int32_t objectId, int32_t alpha, uint32_t duration)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param int32_t alpha: TODO: arg desc
   :param uint32_t duration: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetObjectAlpha(int32_t objectId)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.MoveObjectTo(int32_t objectId, float x, float y, float z, uint32_t duration)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param uint32_t duration: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.MoveObjectBy(int32_t objectId, float x, float y, float z, uint32_t duration)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param uint32_t duration: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetObjectPosition(int32_t objectId, float x, float y, float z)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetObjectPosition(int32_t objectId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.RotateObjectTo(int32_t objectId, float x, float y, float z, float w, uint32_t duration)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param float w: TODO: arg desc
   :param uint32_t duration: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.RotateObjectToEuler(int32_t objectId, float x, float y, float z, uint32_t duration)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param uint32_t duration: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.RotateObjectBy(int32_t objectId, float x, float y, float z, float w, uint32_t duration)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param float w: TODO: arg desc
   :param uint32_t duration: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.RotateObjectByEuler(int32_t objectId, float x, float y, float z, uint32_t duration)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param uint32_t duration: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetObjectRotation(int32_t objectId, float* xOut, float* yOut, float *zOut, float *wOut)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float *zOut: TODO: arg desc
   :param float *wOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetObjectRotationEuler(int32_t objectId, float* xOut, float* yOut, float *zOut)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float *zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetObjectShotReportEnabled(int32_t objectId, uint8_t toggle)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param uint8_t toggle: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint8_t PluginFuncs.IsObjectShotReportEnabled(int32_t objectId)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetObjectTouchedReportEnabled(int32_t objectId, uint8_t toggle)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc
   :param uint8_t toggle: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint8_t PluginFuncs.IsObjectTouchedReportEnabled(int32_t objectId)

   TODO: func desc

   :param int32_t objectId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.
