***************
Weapon settings
***************

.. c:function:: vcmpError PluginFuncs.SetWeaponDataValue(int32_t weaponId, int32_t fieldId, double value)

   TODO: func desc

   :param int32_t weaponId: TODO: arg desc
   :param int32_t fieldId: TODO: arg desc
   :param double value: TODO: arg desc

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: double PluginFuncs.GetWeaponDataValue(int32_t weaponId, int32_t fieldId)

   TODO: func desc

   :param int32_t weaponId: TODO: arg desc
   :param int32_t fieldId: TODO: arg desc

   :return: double TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.ResetWeaponDataValue(int32_t weaponId, int32_t fieldId)

   TODO: func desc

   :param int32_t weaponId: TODO: arg desc
   :param int32_t fieldId: TODO: arg desc

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: uint8_t PluginFuncs.IsWeaponDataValueModified(int32_t weaponId, int32_t fieldId)

   TODO: func desc

   :param int32_t weaponId: TODO: arg desc
   :param int32_t fieldId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.ResetWeaponData(int32_t weaponId)

   TODO: func desc

   :param int32_t weaponId: TODO: arg desc

   :raises: :c:data:`vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: void PluginFuncs.ResetAllWeaponData(void)

   TODO: func desc
