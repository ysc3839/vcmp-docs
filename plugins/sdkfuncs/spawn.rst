************************************
Spawn screen and class configuration
************************************

.. c:function:: int32_t PluginFuncs.AddPlayerClass(int32_t teamId, uint32_t colour, int32_t modelIndex, float x, float y, float z, float angle, int32_t weaponOne, int32_t weaponOneAmmo, int32_t weaponTwo, int32_t weaponTwoAmmo, int32_t weaponThree, int32_t weaponThreeAmmo)

   TODO: func desc

   :param int32_t teamId: TODO: arg desc
   :param uint32_t colour: TODO: arg desc
   :param int32_t modelIndex: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
   :param float angle: TODO: arg desc
   :param int32_t weaponOne: TODO: arg desc
   :param int32_t weaponOneAmmo: TODO: arg desc
   :param int32_t weaponTwo: TODO: arg desc
   :param int32_t weaponTwoAmmo: TODO: arg desc
   :param int32_t weaponThree: TODO: arg desc
   :param int32_t weaponThreeAmmo: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorArgumentOutOfBounds, vcmpErrorPoolExhausted` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: void PluginFuncs.SetSpawnPlayerPosition(float x, float y, float z)

   TODO: func desc

   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

.. c:function:: void PluginFuncs.SetSpawnCameraPosition(float x, float y, float z)

   TODO: func desc

   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

.. c:function:: void PluginFuncs.SetSpawnCameraLookAt(float x, float y, float z)

   TODO: func desc

   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc
