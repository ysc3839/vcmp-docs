****************
Client messaging
****************

.. c:function:: vcmpError PluginFuncs.SendClientScriptData(int32_t playerId, const void* data, size_t size)

   Sends a stream of data to players to be processed by client-side scripts.

   :param int32_t playerId: The ID of the player to send the data to, or -1 to
                            send to all players

   :type data: const void*
   :param data: The byte stream to send

   :param size_t size: The size of the byte stream being sent

   :raises: :c:data:`vcmpErrorNoSuchEntity` if the playerId is not -1 and
            no player with that ID can be found.

            :c:data:`vcmpErrorNullArgument` if *data* is null.

            :c:data:`vcmpErrorTooLargeInput` - reserved. Currently does not
            raise this error under any circumstances.

.. c:function:: vcmpError PluginFuncs.SendClientMessage(int32_t playerId, uint32_t colour, const char* format, ...)

   Sends a message to players to show in the console.

   :param int32_t playerId: The ID of the player to send the message to, or -1
                            to send to all players

   :param uint32_t colour: The base colour for the message in RGBA form.

   :type format: const char*
   :param format: A printf format string

   :param ...: The values to substitute into the format string, if needed

   :raises: :c:data:`vcmpErrorNoSuchEntity` if the playerId is not -1 and
            no player with that ID can be found.

            :c:data:`vcmpErrorNullArgument` if *format* is null.

            :c:data:`vcmpErrorTooLargeInput` if the resulting string is longer
            than 8192 bytes (8 KiB).

.. c:function:: vcmpError PluginFuncs.SendGameMessage(int32_t playerId, int32_t type, const char* format, ...)

   Sends a game message to show text using Vice City's text rendering.

   :param int32_t playerId: The ID of the player to send the message to, or -1
                            to send to all players

   :type format: const char*
   :param format: A printf format string

   :param ...: The values to substitute into the format string, if needed

   :raises: :c:data:`vcmpErrorNoSuchEntity` if the playerId is not -1 and
            no player with that ID can be found.

            :c:data:`vcmpErrorNullArgument` if *format* is null.

            :c:data:`vcmpErrorTooLargeInput` if the resulting string is longer
            than 8192 bytes (8 KiB).

   :see also: :ref:`game-message-types`
