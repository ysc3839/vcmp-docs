***********************
Plugin info and interop
***********************

.. c:function:: uint32_t PluginFuncs.GetServerVersion(void)

   :return: The netcode version of the server.
   :rtype: uint32_t
   
   The server version returned by the plugin API does not correspond to release
   versions like "0.4.6," but is a number that increases monotonically with each
   netcode revision.

   Differences in the hundreds place (e.g. 64500, 64600, 64750) indicate
   breaking changes. Clients with different netcode versions will not be
   able to connect. For example, clients with netcode versions 64400 or 64600
   cannot connect to servers with netcode versions 64500-64599.

.. c:function:: vcmpError PluginFuncs.ExportFunctions(int32_t pluginId, const void** functionList, size_t size)

   Exports a list of function pointers so that other plugins can call into
   functions provided by the current plugin.

   :param int32_t pluginId: The ID of the plugin providing the exported
                            functions. Should be the same as the plugin ID you
                            specify in :c:func:`VcmpPluginInit()`.

   :type functionList:  const void**
   :param functionList: A pointer to an array or struct of data; typically
                        should be function pointers, but can have its own
                        struct elements.

   :param size_t size: The size of the data being exported.

   :raises: :c:data:`vcmpErrorNoSuchEntity` if a plugin matching the given
            *pluginId* cannot be found.


   **Example**

   .. code-block:: c
      :caption: Exporting plugin
      :name: plugin1.c
      :emphasize-lines: 19-21

      typedef struct {
          uint32_t structSize;

          void (*MyExportedPluginFunction)(char* szMessage);
      } MyPluginExports;

      void MyPluginFunction(char* szMessage) {
          if (szMessage != NULL) {
              printf("%s\n", szMessage);
          }
      }

      MyPluginExports exports;

      uint32_t VcmpPluginInit(PluginFuncs* funcs, PluginCallbacks* callbacks, PluginInfo* info) {
          info->name[0] = '\0';
          strncat(info->name, "MySpecialPlugin", sizeof(info->name) - 1);

          exports.structSize = sizeof(MyPluginExports);
          exports.MyExportedPluginFunction = MyPluginFunction;
          funcs->ExportFunctions(info->pluginId, (void**)&exports, sizeof(exports));

          return 1;
      }
    
   .. code-block:: c
      :caption: Importing plugin
      :name: plugin2.c
      :emphasize-lines: 11, 14-15

      typedef struct {
          uint32_t structSize;

          void (*MyExportedPluginFunction)(char* szMessage);
      } MyPluginImports;

      uint32_t VcmpPluginInit(PluginFuncs* funcs, PluginCallbacks* callbacks, PluginInfo* info) {
          int32_t exportPluginId = funcs->FindPlugin("MySpecialPlugin");
          if (exportPluginId != -1) {
              size_t exportSize;
              const void** exports = funcs->GetPluginExports(exportPluginId, &exportSize);

              if (exports != NULL && exportSize == sizeof(MyPluginImports)) {
                  const MyPluginImports* imports = (const MyPluginImports*)exports;
                  imports->MyExportedPluginFunction("Hello, world!");
              }
          }

          return 1;
      }

.. c:function:: uint32_t PluginFuncs.GetNumberOfPlugins(void)

   :return: The number of plugins loaded on the server

.. c:function:: vcmpError PluginFuncs.GetPluginInfo(int32_t pluginId, PluginInfo* pluginInfo)

   Gets information about a plugin given its ID.

   :param int32_t pluginId: The ID of the plugin to get info for

   :param PluginInfo* pluginInfo: A pointer to a :c:type:`PluginInfo` struct
                                  to store the plugin info in.

   :raises: :c:data:`vcmpErrorNoSuchEntity` if no plugin with the given ID can
            be found.

            :c:data:`vcmpErrorNullArgument` if the *pluginInfo* argument is null.

.. c:function:: int32_t PluginFuncs.FindPlugin(const char* pluginName)

   :type pluginName: const char*
   :param pluginName: The name of the plugin to find the ID of

   :return: The ID of the plugin, or -1 if no plugin with that name can be found

.. c:function:: const void** PluginFuncs.GetPluginExports(int32_t pluginId, size_t* exportSize)

   Gets an exported list of functions from another plugin.

   :param int32_t pluginId: The ID of the plugin providing the exported
                            functions.

   :param size_t* size: A pointer to a *size_t* that will hold the size of the
                        plugin's exports if found.
    
   :return: NULL if no exports are present, a pointer to the exports otherwise.

   :raises: :c:data:`vcmpErrorNoSuchEntity` if a plugin matching the given
            *pluginId* cannot be found.

   :see also: :c:func:`PluginFuncs.ExportFunctions()`

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SendPluginCommand(uint32_t commandIdentifier, const char* format, ...)

   Broadcasts a command message to all plugins.

   :param uint32_t commandIdentifier: A unique 32-bit integer identifying what
                                      kind of command is being sent.

   :type format: const char*
   :param format: A printf format string.

   :param ...: The data to substitute into the format string, if needed.

   :raises: :c:data:`vcmpErrorNullArgument` if *format* is null.

            :c:data:`vcmpErrorTooLargeInput` if the resulting string is longer
            than 8192 bytes (8KiB).

   :see also: :c:func:`PluginCallbacks.OnPluginCommand()`

.. c:function:: uint64_t PluginFuncs.GetTime(void)

   :return: The number of milliseconds elapsed since the system started. Not
            suitable for getting wall time.

.. c:function:: vcmpError PluginFuncs.LogMessage(const char* format, ...)

   Logs a message to the server console and to *serverlog.txt*.

   :type format: const char*
   :param format: A printf format string.

   :param ...: The values to substitute into the format string, if needed.

   :raises: :c:data:`vcmpErrorNullArgument` if *format* is null.

            :c:data:`vcmpErrorTooLargeInput` if the resulting string is longer
            than 8192 bytes (8KiB).

.. c:function:: vcmpError PluginFuncs.GetLastError(void)

   :return: The last error raised by a plugin function call.
