****************
Player functions
****************

Player information
==================

.. c:function:: int32_t PluginFuncs.GetPlayerIdFromName(const char* name)

   TODO: func desc

   :param const char* name: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`-1 == vcmpEntityNone` if TODO

.. c:function:: uint8_t PluginFuncs.IsPlayerConnected(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: uint8_t PluginFuncs.IsPlayerStreamedForPlayer(int32_t checkedPlayerId, int32_t playerId)

   TODO: func desc

   :param int32_t checkedPlayerId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint32_t PluginFuncs.GetPlayerKey(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint32_t TODO: ret desc
   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetPlayerName(int32_t playerId, char* buffer, size_t size)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param char* buffer: TODO: arg desc
   :param size_t size: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorNullArgument, vcmpErrorBufferTooSmall` if TODO

.. c:function:: vcmpError PluginFuncs.SetPlayerName(int32_t playerId, const char* name)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param const char* name: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorNullArgument, vcmpErrorInvalidName, vcmpErrorTooLargeInput` if TODO

.. c:function:: vcmpPlayerState PluginFuncs.GetPlayerState(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: vcmpPlayerState TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerOption(int32_t playerId, vcmpPlayerOption option, uint8_t toggle)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param vcmpPlayerOption option: TODO: arg desc
   :param uint8_t toggle: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: uint8_t PluginFuncs.GetPlayerOption(int32_t playerId, vcmpPlayerOption option)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param vcmpPlayerOption option: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Player worlds
=============

.. c:function:: vcmpError PluginFuncs.SetPlayerWorld(int32_t playerId, int32_t world)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t world: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerWorld(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerSecondaryWorld(int32_t playerId, int32_t secondaryWorld)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t secondaryWorld: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerSecondaryWorld(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerUniqueWorld(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: uint8_t PluginFuncs.IsPlayerWorldCompatible(int32_t playerId, int32_t world)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t world: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Player class settings
=====================

.. c:function:: int32_t PluginFuncs.GetPlayerClass(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerTeam(int32_t playerId, int32_t teamId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t teamId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerTeam(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerSkin(int32_t playerId, int32_t skinId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t skinId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerSkin(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerColour(int32_t playerId, uint32_t colour)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param uint32_t colour: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint32_t PluginFuncs.GetPlayerColour(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Player spawn cycle
==================

.. c:function:: uint8_t PluginFuncs.IsPlayerSpawned(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.ForcePlayerSpawn(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.ForcePlayerSelect(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: void PluginFuncs.ForceAllSelect(void)

   TODO: func desc


.. c:function:: uint8_t PluginFuncs.IsPlayerTyping(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Player statistics
=================

.. c:function:: vcmpError PluginFuncs.GivePlayerMoney(int32_t playerId, int32_t amount)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t amount: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetPlayerMoney(int32_t playerId, int32_t amount)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t amount: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerMoney(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerScore(int32_t playerId, int32_t score)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t score: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerScore(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerWantedLevel(int32_t playerId, int32_t level)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t level: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerWantedLevel(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerPing(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: double PluginFuncs.GetPlayerFPS(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: double TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Player health and immunity
==========================

.. c:function:: vcmpError PluginFuncs.SetPlayerHealth(int32_t playerId, float health)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float health: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: float PluginFuncs.GetPlayerHealth(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: float TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerArmour(int32_t playerId, float armour)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float armour: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: float PluginFuncs.GetPlayerArmour(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: float TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerImmunityFlags(int32_t playerId, uint32_t flags)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param uint32_t flags: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint32_t PluginFuncs.GetPlayerImmunityFlags(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Player position and rotation
============================

.. c:function:: vcmpError PluginFuncs.SetPlayerPosition(int32_t playerId, float x, float y, float z)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetPlayerPosition(int32_t playerId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetPlayerSpeed(int32_t playerId, float x, float y, float z)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetPlayerSpeed(int32_t playerId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.AddPlayerSpeed(int32_t playerId, float x, float y, float z)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float x: TODO: arg desc
   :param float y: TODO: arg desc
   :param float z: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetPlayerHeading(int32_t playerId, float angle)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float angle: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: float PluginFuncs.GetPlayerHeading(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: float TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerAlpha(int32_t playerId, int32_t alpha, uint32_t fadeTime)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t alpha: TODO: arg desc
   :param uint32_t fadeTime: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerAlpha(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.GetPlayerAimPosition(int32_t playerId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.GetPlayerAimDirection(int32_t playerId, float* xOut, float* yOut, float* zOut)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float* xOut: TODO: arg desc
   :param float* yOut: TODO: arg desc
   :param float* zOut: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

Player actions
==============

.. c:function:: uint8_t PluginFuncs.IsPlayerOnFire(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: uint8_t PluginFuncs.IsPlayerCrouching(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerAction(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: uint32_t PluginFuncs.GetPlayerGameKeys(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Player vehicles
===============

.. c:function:: vcmpError PluginFuncs.PutPlayerInVehicle(int32_t playerId, int32_t vehicleId, int32_t slotIndex, uint8_t makeRoom, uint8_t warp)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t vehicleId: TODO: arg desc
   :param int32_t slotIndex: TODO: arg desc
   :param uint8_t makeRoom: TODO: arg desc
   :param uint8_t warp: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds, vcmpErrorRequestDenied` if TODO

.. c:function:: vcmpError PluginFuncs.RemovePlayerFromVehicle(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpPlayerVehicle PluginFuncs.GetPlayerInVehicleStatus(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: vcmpPlayerVehicle TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerInVehicleSlot(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerVehicleId(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Player weapons
==============

.. c:function:: vcmpError PluginFuncs.GivePlayerWeapon(int32_t playerId, int32_t weaponId, int32_t ammo)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t weaponId: TODO: arg desc
   :param int32_t ammo: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: vcmpError PluginFuncs.SetPlayerWeapon(int32_t playerId, int32_t weaponId, int32_t ammo)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t weaponId: TODO: arg desc
   :param int32_t ammo: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerWeapon(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerWeaponAmmo(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.SetPlayerWeaponSlot(int32_t playerId, int32_t slot)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t slot: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerWeaponSlot(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerWeaponAtSlot(int32_t playerId, int32_t slot)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t slot: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerAmmoAtSlot(int32_t playerId, int32_t slot)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t slot: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity, vcmpErrorArgumentOutOfBounds` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.RemovePlayerWeapon(int32_t playerId, int32_t weaponId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t weaponId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.RemoveAllWeapons(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

Player camera
=============

.. c:function:: vcmpError PluginFuncs.SetCameraPosition(int32_t playerId, float posX, float posY, float posZ, float lookX, float lookY, float lookZ)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param float posX: TODO: arg desc
   :param float posY: TODO: arg desc
   :param float posZ: TODO: arg desc
   :param float lookX: TODO: arg desc
   :param float lookY: TODO: arg desc
   :param float lookZ: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.RestoreCamera(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: uint8_t PluginFuncs.IsCameraLocked(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

Miscellaneous player functions
==============================

.. c:function:: vcmpError PluginFuncs.SetPlayerAnimation(int32_t playerId, int32_t groupId, int32_t animationId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t groupId: TODO: arg desc
   :param int32_t animationId: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: int32_t PluginFuncs.GetPlayerStandingOnVehicle(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerStandingOnObject(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: uint8_t PluginFuncs.IsPlayerAway(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: int32_t PluginFuncs.GetPlayerSpectateTarget(int32_t playerId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc

   :return: int32_t TODO: ret desc
   :raises: :c:data:`vcmpErrorNoSuchEntity` if TODO

.. c:function:: vcmpError PluginFuncs.SetPlayerSpectateTarget(int32_t playerId, int32_t targetId)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t targetId: TODO: arg desc

   :raises: :c:data:`GetLastError: vcmpErrorNoSuchEntity` if TODO

   .. note::

      You must call :c:func:`PluginFuncs.GetLastError()` to check if an error
      condition was raised.

.. c:function:: vcmpError PluginFuncs.RedirectPlayerToServer(int32_t playerId, const char* ip, uint32_t port, const char* nick, const char* serverPassword, const char* userPassword)

   TODO: func desc

   :param int32_t playerId: TODO: arg desc
   :param const char* ip: TODO: arg desc
   :param uint32_t port: TODO: arg desc
   :param const char* nick: TODO: arg desc
   :param const char* serverPassword: TODO: arg desc
   :param const char* userPassword: TODO: arg desc

   :raises: :c:data:`vcmpErrorNoSuchEntity, vcmpErrorNullArgument` if TODO
