*******************
Event documentation
*******************

.. c:function:: uint8_t PluginCallbacks.OnServerInitialise(void)

   TODO: event desc

   :return: uint8_t TODO: ret desc
.. c:function:: void PluginCallbacks.OnServerShutdown(void)

   TODO: event desc

.. c:function:: void PluginCallbacks.OnServerFrame(float elapsedTime)

   TODO: event desc

   :param float elapsedTime: TODO: arg desc

.. c:function:: uint8_t PluginCallbacks.OnPluginCommand(uint32_t commandIdentifier, const char* message)

   TODO: event desc

   :param uint32_t commandIdentifier: TODO: arg desc
   :param const char* message: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: uint8_t PluginCallbacks.OnIncomingConnection(char* playerName, size_t nameBufferSize, const char* userPassword, const char* ipAddress)

   TODO: event desc

   :param char* playerName: TODO: arg desc
   :param size_t nameBufferSize: TODO: arg desc
   :param const char* userPassword: TODO: arg desc
   :param const char* ipAddress: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: void PluginCallbacks.OnClientScriptData(int32_t playerId, const uint8_t* data, size_t size)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param const uint8_t* data: TODO: arg desc
   :param size_t size: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerConnect(int32_t playerId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerDisconnect(int32_t playerId, vcmpDisconnectReason reason)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param vcmpDisconnectReason reason: TODO: arg desc

.. c:function:: uint8_t PluginCallbacks.OnPlayerRequestClass(int32_t playerId, int32_t offset)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t offset: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: uint8_t PluginCallbacks.OnPlayerRequestSpawn(int32_t playerId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: void PluginCallbacks.OnPlayerSpawn(int32_t playerId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerDeath(int32_t playerId, int32_t killerId, int32_t reason, vcmpBodyPart bodyPart)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t killerId: TODO: arg desc
   :param int32_t reason: TODO: arg desc
   :param vcmpBodyPart bodyPart: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerUpdate(int32_t playerId, vcmpPlayerUpdate updateType)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param vcmpPlayerUpdate updateType: TODO: arg desc

.. c:function:: uint8_t PluginCallbacks.OnPlayerRequestEnterVehicle(int32_t playerId, int32_t vehicleId, int32_t slotIndex)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t vehicleId: TODO: arg desc
   :param int32_t slotIndex: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: void PluginCallbacks.OnPlayerEnterVehicle(int32_t playerId, int32_t vehicleId, int32_t slotIndex)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t vehicleId: TODO: arg desc
   :param int32_t slotIndex: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerExitVehicle(int32_t playerId, int32_t vehicleId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t vehicleId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerNameChange(int32_t playerId, const char* oldName, const char* newName)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param const char* oldName: TODO: arg desc
   :param const char* newName: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerStateChange(int32_t playerId, vcmpPlayerState oldState, vcmpPlayerState newState)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param vcmpPlayerState oldState: TODO: arg desc
   :param vcmpPlayerState newState: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerActionChange(int32_t playerId, int32_t oldAction, int32_t newAction)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t oldAction: TODO: arg desc
   :param int32_t newAction: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerOnFireChange(int32_t playerId, uint8_t isOnFire)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param uint8_t isOnFire: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerCrouchChange(int32_t playerId, uint8_t isCrouching)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param uint8_t isCrouching: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerGameKeysChange(int32_t playerId, uint32_t oldKeys, uint32_t newKeys)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param uint32_t oldKeys: TODO: arg desc
   :param uint32_t newKeys: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerBeginTyping(int32_t playerId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerEndTyping(int32_t playerId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerAwayChange(int32_t playerId, uint8_t isAway)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param uint8_t isAway: TODO: arg desc

.. c:function:: uint8_t PluginCallbacks.OnPlayerMessage(int32_t playerId, const char* message)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param const char* message: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: uint8_t PluginCallbacks.OnPlayerCommand(int32_t playerId, const char* message)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param const char* message: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: uint8_t PluginCallbacks.OnPlayerPrivateMessage(int32_t playerId, int32_t targetPlayerId, const char* message)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t targetPlayerId: TODO: arg desc
   :param const char* message: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: void PluginCallbacks.OnPlayerKeyBindDown(int32_t playerId, int32_t bindId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t bindId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerKeyBindUp(int32_t playerId, int32_t bindId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t bindId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerSpectate(int32_t playerId, int32_t targetPlayerId)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param int32_t targetPlayerId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPlayerCrashReport(int32_t playerId, const char* report)

   TODO: event desc

   :param int32_t playerId: TODO: arg desc
   :param const char* report: TODO: arg desc

.. c:function:: void PluginCallbacks.OnVehicleUpdate(int32_t vehicleId, vcmpVehicleUpdate updateType)

   TODO: event desc

   :param int32_t vehicleId: TODO: arg desc
   :param vcmpVehicleUpdate updateType: TODO: arg desc

.. c:function:: void PluginCallbacks.OnVehicleExplode(int32_t vehicleId)

   TODO: event desc

   :param int32_t vehicleId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnVehicleRespawn(int32_t vehicleId)

   TODO: event desc

   :param int32_t vehicleId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnObjectShot(int32_t objectId, int32_t playerId, int32_t weaponId)

   TODO: event desc

   :param int32_t objectId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc
   :param int32_t weaponId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnObjectTouched(int32_t objectId, int32_t playerId)

   TODO: event desc

   :param int32_t objectId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

.. c:function:: uint8_t PluginCallbacks.OnPickupPickAttempt(int32_t pickupId, int32_t playerId)

   TODO: event desc

   :param int32_t pickupId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

   :return: uint8_t TODO: ret desc
.. c:function:: void PluginCallbacks.OnPickupPicked(int32_t pickupId, int32_t playerId)

   TODO: event desc

   :param int32_t pickupId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnPickupRespawn(int32_t pickupId)

   TODO: event desc

   :param int32_t pickupId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnCheckpointEntered(int32_t checkPointId, int32_t playerId)

   TODO: event desc

   :param int32_t checkPointId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnCheckpointExited(int32_t checkPointId, int32_t playerId)

   TODO: event desc

   :param int32_t checkPointId: TODO: arg desc
   :param int32_t playerId: TODO: arg desc

.. c:function:: void PluginCallbacks.OnEntityPoolChange(vcmpEntityPool entityType, int32_t entityId, uint8_t isDeleted)

   TODO: event desc

   :param vcmpEntityPool entityType: TODO: arg desc
   :param int32_t entityId: TODO: arg desc
   :param uint8_t isDeleted: TODO: arg desc

.. c:function:: void PluginCallbacks.OnServerPerformanceReport(size_t entryCount, const char** descriptions, uint64_t* times)

   TODO: event desc

   :param size_t entryCount: TODO: arg desc
   :param const char** descriptions: TODO: arg desc
   :param uint64_t* times: TODO: arg desc
